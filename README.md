# Simple comparison of AI models.

This script tests accuracy of 3 AI models predictions, using 3 libraries:
- liblinear-ruby
- rb-libsvm
- ruby-fann

All clasifficators use same data

NOTE: LibSVM can be optimized to achieve similar accuracy to neural network, by adjusting C and gamma parameters.

## Usage
- `bundle install`
- `ruby main.rb`