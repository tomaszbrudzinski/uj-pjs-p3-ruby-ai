require 'liblinear'
require_relative '../classificator'

class LinearClassificator < Classificator

  def run
    puts "=== Linear Classificator ==============================================================="

    model = Liblinear.train(
        {solver_type: Liblinear::L2R_LR},
        @training_y_data,
        @training_x_data,
        100
    )

    # Predict single class
    prediction = Liblinear.predict(model, [45, 45 ** 2, 85, 85 ** 2, 45 * 85])
    puts "Algorithm predicted class #{prediction}"

    @test_x_data.each do |params|
      @predicted.push(Liblinear.predict(model, params))
    end

    present_results
  end

end
