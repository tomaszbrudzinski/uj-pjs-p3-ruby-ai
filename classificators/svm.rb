require 'libsvm'
require_relative '../classificator'

class SvmClassificator < Classificator

  def run
    puts "=== SVM Classificator ==============================================================="

    # Prepare arrays for libsvm
    @test_x_data = @test_x_data.map { |feature_row| Libsvm::Node.features(feature_row) }
    @training_x_data = @training_x_data.map { |feature_row| Libsvm::Node.features(feature_row) }

    parameter = Libsvm::SvmParameter.new
    parameter.cache_size = 1 # in megabytes
    parameter.eps = 0.001
    parameter.c = 1
    parameter.gamma = 0.01
    parameter.kernel_type = Libsvm::KernelType::RBF

    problem = Libsvm::Problem.new
    problem.set_examples(@training_y_data, @training_x_data)

    model = Libsvm::Model.train(problem, parameter)

    # Predict single class
    prediction = model.predict(Libsvm::Node.features([45, 85]))
    puts "Algorithm predicted class: #{prediction.inspect}"

    @test_x_data.each do |params|
      @predicted.push(model.predict(params))
    end

    present_results
  end

end
