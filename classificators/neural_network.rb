require 'ruby-fann'
require_relative '../classificator'

class NeuralNetworkClassificator < Classificator

  def run
    puts "=== Neural Network Classificator ==============================================================="

    # Setup training data
    train = RubyFann::TrainData.new(:inputs => @training_x_data, :desired_outputs => @training_y_data);

    # Setup model and train using training data
    model = RubyFann::Standard.new(
        num_inputs: 2,
        hidden_neurons: [6],
        num_outputs: 1
    )
    model.train_on_data(train, 5000, 500, 0.01)

    # Predict single class
    prediction = model.run([45, 85])
    puts "Algorithm predicted class: #{prediction.map { |e| e.round }}"

    @test_x_data.each do |params|
      @predicted.push(model.run(params).map { |e| e.round })
    end

    present_results
  end
end

