require 'csv'
require_relative 'classificators/linear'
require_relative 'classificators/svm'
require_relative 'classificators/neural_network'

x_data = []
y_2d_data = []
y_1d_data = []
# Load data from CSV file into arrays
CSV.foreach("./data/admission.csv", :headers => false) do |row|
  x_data.push([row[0].to_f, row[1].to_f])
  y_2d_data.push([row[2].to_i])
  y_1d_data.push(row[2].to_i)
end

# Divide data into a training set and test set
test_size_percentange = 20.0 # 20.0%
test_set_size = x_data.size * (test_size_percentange / 100.to_f)

test_x_data = x_data[0..(test_set_size - 1)]
test_y_2d_data = y_2d_data[0..(test_set_size - 1)]
test_y_1d_data = y_1d_data[0..(test_set_size - 1)]

training_x_data = x_data[test_set_size..x_data.size]
training_y_2d_data = y_2d_data[test_set_size..y_2d_data.size]
training_y_1d_data = y_1d_data[test_set_size..y_1d_data.size]

# Run classificators
linearClassificator = LinearClassificator.new(test_size_percentange, test_set_size, test_x_data, test_y_1d_data, training_x_data, training_y_1d_data)
linearClassificator.run

svmClassificator = SvmClassificator.new(test_size_percentange, test_set_size, test_x_data, test_y_1d_data, training_x_data, training_y_1d_data)
svmClassificator.run

neuralNetworkClassificator = NeuralNetworkClassificator.new(test_size_percentange, test_set_size, test_x_data, test_y_2d_data, training_x_data, training_y_2d_data)
neuralNetworkClassificator.run
