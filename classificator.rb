class Classificator

  def initialize(test_size_percentange, test_set_size, test_x_data, test_y_data, training_x_data, training_y_data)
    @test_size_percentange = test_size_percentange
    @test_set_size = test_set_size
    @test_x_data = test_x_data
    @test_y_data = test_y_data
    @training_x_data = training_x_data
    @training_y_data = training_y_data
    @predicted = []
  end

  def present_results
    correct = @predicted.collect.with_index { |e, i| (e == @test_y_data[i]) ? 1 : 0 }.inject { |sum, e| sum + e }
    puts "Classification accuracy: #{((correct.to_f / @test_set_size) * 100).round(2)}% - test set of size #{@test_size_percentange}%"
    puts
  end

end
